(ns tokyo.amphetamine.hellodroid.main
    (:require [neko.activity :refer [defactivity set-content-view!]]
              [neko.debug :refer [*a]]
              [neko.intent :refer [intent put-extras]]
              [neko.log :as log]
              [neko.notify :refer [toast]]
              [neko.resource :as res]
              [neko.find-view :refer [find-view]]
              [neko.threading :refer [on-ui]])
    (:import android.widget.EditText
             android.bluetooth.BluetoothAdapter
             android.bluetooth.BluetoothDevice))

;; We execute this function to import all subclasses of R class. This gives us
;; access to all application resources.
(res/import-all)

(def REQUEST_ENABLE_BT 1357)

(defn get-bluetooth-adapter
  "Gets the bluetooth adapter if the hardware supports it. Returns the adapter
  or nil."
  []
  (BluetoothAdapter/getDefaultAdapter)) ;; TODO: needs to check target version
                                        ;; and use other function if so

(defn enable-bluetooth
  "Turns on the bluetooth adapter. Returns it again, or nil if it couldn't find
  one."
  []
  (let [b-adapter (get-bluetooth-adapter)]
    (if (nil? b-adapter)
      (do (on-ui (toast "Couldn't find any Bluetooth devices.")) nil)
      (do
        (if (not (.isEnabled b-adapter))
          (on-ui ;; NOTE: Needs to be on a thread. Using UI for now 'til I puzzle
                 ;; out the others.
           (let [enable-b-intent (intent BluetoothAdapter/ACTION_REQUEST_ENABLE {}) ]
             (. (*a) startActivityForResult
                enable-b-intent REQUEST_ENABLE_BT))))
        b-adapter))))

(defn notify-from-edit
  "Finds an EditText element with ID ::user-input in the given activity. Gets
  its contents and displays them in a toast if they aren't empty. We use
  resources declared in res/values/strings.xml."
  [activity]
  (let [^EditText input (.getText (find-view activity ::user-input))]
    (toast (if (empty? input)
             (res/get-string R$string/input_is_empty)
             (res/get-string R$string/your_input_fmt input))
           :long)))

;; This is how an Activity is defined. We create one and specify its onCreate
;; method. Inside we create a user interface that consists of an edit and a
;; button. We also give set callback to the button.
(defactivity tokyo.amphetamine.hellodroid.MainActivity
  :key :main
  (onCreate [this bundle]
    (.superOnCreate this bundle)
    (neko.debug/keep-screen-on this)
    (on-ui
     (set-content-view!
      (*a)
      [:linear-layout {:orientation :vertical
                       :background-color (android.graphics.Color/parseColor
                                          "#112030")
                       :layout-width :fill
                       :layout-height :wrap}
       [:edit-text {:id ::user-input
                    :hint "Type text here"
                    :layout-width :fill}]
       [:button {:text R$string/touch_me ;; We use resource here, but could
                                         ;; have used a plain string too.
                 :on-click (fn [_] (notify-from-edit (*a)))}]
       [:button {:text "Enable bluetooth"
                 :on-click (fn [_] (enable-bluetooth))}]]))
    ))
