# HelloDroid

This is a Clojure/Android application to figure out Bluetooth access.

## Starting out

### Clojure/Android enviroment

First you need to get [`lein`](https://www.leiningen.org). You also need
[`adb`](https://developer.android.com/studio/command-line/adb.html). 

If you want to try the app out on a virtual device, you'll also need
[Android Studio](https://developer.android.com/studio/). (There's a bunch of
instructions on setting up emulators there.) But! *Android emulators do not
support Bluetooth*--so you'll only be able to see that the app runs.

Otherwise you can just plug in your Android device and start `adb devices -l` in
a terminal to make sure you're all hooked up. (The device should prompt you for
debug access if you've got it set up in the development options.)

### Building

Once you've got all that sorted out, checkout the project, and `cd
hello-droid`.

You need to go into [project.clj](./project.clj) and change the line that says

```clj
:android {;; Specify the path to the Android SDK directory.
            :sdk-path "/home/user/tools/Android/Sdk/"
```

to the path where you installed the Android Studio SDK.

Ok. Now you can run `lein doall`.

## Sweet references

The [clojure-android](https://github.com/clojure-android/) project is totally
sweet. Especially the [Neko](https://github.com/clojure-android/neko/)
framework.

## License

AGPLv3
